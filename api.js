let platformClient = require('purecloud-platform-client-v2');

let client = platformClient.ApiClient.instance;
let env = process.env['ENV'] || 'mypurecloud.ie';
let clientId =
  process.env['CLIENT_ID'] || '10d2a823-255c-4461-a207-2f3d4fe1d2af';
let clientSecret =
  process.env['CLIENT_SECRET'] || 'dwfWGEzdPeGfYUiLeZXGR1GQcZyKcgBOMekYxLZ-nsw';

// set PureCloud environment
client.setEnvironment(env);

// Login to the PureCloud and retreive Users and Queues
function login() {
  console.log('login PureCloud()');
  console.log('PureCloud auth ---');
  console.log(`env         : ${env}`);
  console.log(`clientId    : ${clientId}`);
  console.log(`clientSecret: ${clientSecret.substring(0, 10)}(...)`);

  return new Promise(function (resolve, reject) {
    try {
      client
        .loginClientCredentialsGrant(clientId, clientSecret)
        .then(function () {
          console.log('service is connected to PureCloud!');

          let resp = {
            token: client.authData.accessToken,
            expiryTime: client.authData.tokenExpiryTime,
          };
          client.setAccessToken(resp.token);
          resolve(resp);
        })
        .catch(function (err) {
          console.log(err);
          reject();
        });
    } catch (err) {
      console.log(err);
      reject();
    }
  });
}

// https://developer.mypurecloud.ie/developer-tools/#/api-explorer?share=N4IgDghgLgFiBc4D2BnKBhJA7AbgUwCcVoBLbFAWTxWIHNqBBerKAG2pRABoQBbPWEgAmCZGm4gArgRKiA9BDAk5OAExyAxtnxFS5OfxoR6KBczYcJkAhF4A1CK0nUEoAEbCAnqOAAdLAAEQb4gAGYESLwMQkIEHCHwASEhXP5BSSBQSNGx8QgZKWnBmdkxcTRUNHhY9AQAKp5geAkF3EUZUHgAHlAAQl4tySD+AL4gIyNAA
// Data Action (Emeabilling)
// https://apps.mypurecloud.ie/directory/#/admin/integrations/actions/custom_-_7bb880bb-b8cf-4e17-9eaa-1ff7423ddbd7/setup/test
function sendAgentlessSMS(_to, _body) {
  console.log('sendAgentlessSMS');
  return new Promise(function (resolve, reject) {
    let apiInstance = new platformClient.ConversationsApi();
    let body = {
      fromAddress: '+447480536700',
      toAddress: _to,
      toAddressMessengerType: 'sms',
      textBody: _body,
    };

    apiInstance
      .postConversationsMessagesAgentless(body)
      .then((data) => {
        console.log(
          `postConversationsMessagesAgentless success! data: ${JSON.stringify(
            data,
            null,
            2
          )}`
        );
        resolve();
      })
      .catch((err) => {
        console.log(
          'There was a failure calling postConversationsMessagesAgentless'
        );
        console.error(err);
        reject();
      });
  });
}

function getOutboundSequences() {
  return new Promise(function (resolve, reject) {
    let apiInstance = new platformClient.OutboundApi();

    let opts = {
      pageSize: 100,
      pageNumber: 1,
    };

    apiInstance
      .getOutboundSequences(opts)
      .then(async (data) => {
        if (Object.entries(data.entities).length > 0) {
          console.log(data);
        }
        resolve();
      })
      .catch((err) => {
        console.error('Error while calling getOutboundSequences:', err);
        reject(err);
      });
  });
}

function sendMail() {
  return new Promise(function (resolve, reject) {
    let apiInstance = new platformClient.ConversationsApi();

    let body = {
      queueId: '21cb5843-5a7e-4d36-a4b0-0268c5b2d242',
      provider: 'OTHER',
      toAddress: 'daniel.szlaski@genesys.com',
      toName: 'Test',
      fromAddress: 'Sender1@internal.pl',
      fromName: 'Sender1',
      subject: 'Test',
      direction: 'INBOUND',
      textBody: 'Sample txt',
      htmlBody: 'This is HTML Body',
    };

    apiInstance
      .postConversationsEmails(body)
      .then((data) => {
        console.log(
          `postConversationsEmails success! data: ${JSON.stringify(
            data,
            null,
            2
          )}`
        );
        resolve();
      })
      .catch((err) => {
        console.log('There was a failure calling postConversationsEmails');
        console.error(err);
        reject();
      });
  });
}

function getUsers() {
  return new Promise(function (resolve, reject) {
    let apiInstance = new platformClient.UsersApi();

    let opts = {
      pageSize: 100, // Number | Page size
      pageNumber: 1, // Number | Page number
      expand: ['skills'], // [String] | Which fields, if any, to expand
      state: 'active', // String | Only list users of this state
    };

    apiInstance
      .getUsers(opts)
      .then((data) => {
        resolve(data.entities);
      })
      .catch((err) => {
        console.log('There was a failure calling getUsers');
        console.error(err);
        reject(err);
      });
  });
}

module.exports = {
  login,
  sendAgentlessSMS,
  getOutboundSequences,
  sendMail,
  getUsers,
};
