let api = require('./api');

api
  .login()
  .then(function (resp) {
    console.log(`done -> (${JSON.stringify(resp)}`);

    api
      .getUsers()
      .then(function (resp) {
        // We've got users , build User/Skill object
        let skills = {};
        /*
        skills = {
          'a7964a42-252d-4a27-af4b-23aa0d3927b7': [],
          'd89f2adb-7737-48d4-aa30-793e87be54a5': [
          {
            skillId: '98d68b1c-13ab-4dee-a7b3-45c9ffa15666',
            skillName: 'Node JS Master'
          }
          ]
        }
        */

        // Go for every User definision one-by-one
        resp.forEach((user) => {
          let skillsForUser = [];
          // Check if skill object exists, if true -> enum it
          if (user.skills) {
            // For every skill for this user, read it
            user.skills.forEach((skill) => {
              skillsForUser.push({
                skillId: skill.id,
                skillName: skill.name,
              });
            });
            // Add skills to user definition
            skills[user.id] = skillsForUser;
          }
        });

        // List all items:
        console.log(skills);
        // Get Skills for userId:
        console.log(skills['d89f2adb-7737-48d4-aa30-793e87be54a5']);
      })
      .catch(function (err) {
        console.log(err);
      });
  })
  .catch(function (err) {
    console.log(err);
  });
